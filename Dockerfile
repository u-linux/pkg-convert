FROM alpine:edge

RUN apk update && \
    apk add --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing \
      git make mustach shellcheck

WORKDIR /usr/src/pkg-convert
COPY . /usr/src/pkg-convert

RUN make install

ENTRYPOINT ["/usr/local/bin/pkg-convert"]
