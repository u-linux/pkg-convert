INSTALL = /usr/bin/install
INSTALL_DATA = ${INSTALL} -m 644
SHELLCHECK = shellcheck
YAMLLINT = yamllint

PROGRAM_NAME = pkg-convert

TARGET_DIR = .
BUILD_DIR = ${TARGET_DIR}
SRC_DIR = ${TARGET_DIR}/src

PREFIX = /usr/local
BIN_DIR = ${PREFIX}/bin
SHARE_DIR = ${PREFIX}/share/${PROGRAM_NAME}

SRC = ${SRC_DIR}/converter.sh

all: ${PROGRAM_NAME}

${PROGRAM_NAME}:
	@cp ${SRC} ${BUILD_DIR}/$@
	@chmod +x ${BUILD_DIR}/$@

install: ${PROGRAM_NAME}
	@${INSTALL} ${BUILD_DIR}/${PROGRAM_NAME} ${BIN_DIR}
	@mkdir -p ${SHARE_DIR}
	@cp -R ${TARGET_DIR}/template ${SHARE_DIR}/

check:
	@${SHELLCHECK} ${SRC}
	@${YAMLLINT} ${TARGET_DIR}/template/schema.yml

clean:
	@rm -rf ${BUILD_DIR}/${PROGRAM_NAME}
