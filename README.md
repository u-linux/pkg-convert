# pkg-convert

_Convert between various package formats._

## Motivation

The primary goal of this project is to create a simple utility that will allow
for interoperability between the various package managers. This will save
package maintainers time and will make it easier to integrate different
UNIX-like operating systems. 

**Note: this is specifically for converting package sources, not binary packages.**

## Dependencies

- [libfyaml](https://github.com/pantoniou/libfyaml)

- [mustach](https://gitlab.com/jobol/mustach)

## Installation

```sh
make install
```

## License

SPDX-License-Identifier: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

# See Also

- [Alien](https://en.wikipedia.org/wiki/Alien_(file_converter))

